###
# Running this script:
#
# (1) Create & activate virtual environment
# `virtualenv -p python3 venv && source venv/bin/activate`
#
# (2) Install required packages
# `python -m pip install -r requirements.txt`
#
# (3) Execute it
# `python main.py`
#
#
# The only external dependency is `click`, which describes itself as
# "Python composable command line interface toolkit"
#
# For more information,
# see https://github.com/pallets/click
###

import csv
import random
import click


# Create data array
data = {}

while True:
    # Start fresh round by defining ..
    # (1) .. institution
    key = click.prompt('Ausbildungsstätte')

    # (2) .. number of available seats
    limit = click.prompt('Verfügbare Plätze', default=1)

    # Define buffer
    people = []

    while True:
        # Add participant ..
        if click.confirm('Teilnehmer hinzufügen', default=True):
            # .. by name
            people.append(click.prompt('Name'))

        # .. otherwise ..
        else:
            # .. proceed with next step
            break

    # Get random participants by ..
    # (1) .. shuffling them
    random.shuffle(people)

    # (2) .. limiting their number
    data[key] = people[:limit]

    # If another round isn't required ..
    if not click.confirm('Neue Runde', default=True):
        # .. abort execution
        break

# Save data file
file = 'verwaltungsstation.csv'

click.echo('Speichern als %s ..' % file, nl=False)

# Store data ..
with open(file, 'w') as csv_file:
    # (1) .. create writer object
    writer = csv.writer(csv_file, quoting=csv.QUOTE_ALL)

    # (2) .. write table rows
    writer.writerow(data.keys())
    writer.writerow([', '.join(value) for value in data.values()])

# Goodbye!
click.echo(' abgeschlossen!')
